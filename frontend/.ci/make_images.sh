#!/usr/bin/env bash
set -euo pipefail

REGISTRY_ROOT=registry.gitlab.com/haveneer/bealy-ci-demo

for dir in node:12-alpine-jq \
           node:16-alpine-jq \
           node:12-jq \
           node:16-jq
do
  pushd $dir
  docker build -t ${REGISTRY_ROOT}/$dir .
  docker push ${REGISTRY_ROOT}/$dir
  popd
done