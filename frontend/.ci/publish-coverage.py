import urllib.request
import tarfile
import json
import os
import shutil
import logging, sys
from enum import Enum, unique

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logging.debug(f"Job Status is {os.environ.get('CI_JOB_STATUS')}")

CONTENT="content.tgz"
PUBLIC="public"
DATABASE="database.json"
SHIELD_BADGE_URL="https://img.shields.io/badge"

# Load environment variable or debug values
CI_PAGES_URL=os.environ.get('CI_PAGES_URL') or "https://haveneer-training.gitlab.io/gitlab-ci-demo/"
CI_COMMIT_REF_NAME=os.environ.get('CI_COMMIT_REF_NAME') or "master"
CI_COMMIT_TIMESTAMP=os.environ.get('CI_COMMIT_TIMESTAMP') or "now"
CI_COMMIT_SHA=os.environ.get('CI_COMMIT_SHA') or "0"*40
CI_COMMIT_MESSAGE=os.environ.get('CI_COMMIT_MESSAGE') or "Commit message [DEBUG]\n\nSHOULD BE DISCARDED"
CI_COMMIT_MESSAGE=CI_COMMIT_MESSAGE.split('\n', 1)[0]

@unique
class State(int, Enum):
    OK: int = 1
    BETTER: int = 2
    WORSE: int = 3
    FAILURE: int = 4

try:
    response = urllib.request.urlopen(f"{CI_PAGES_URL}/{CONTENT}")
    with open(CONTENT, 'wb') as file:
        file.write(response.read())
    tar = tarfile.open(CONTENT)
    tar.extractall()
    tar.close()
except BaseException as err:
    # private pages should use something more
    # cf https://gitlab.com/gitlab-org/gitlab-pages/-/issues/388
    logging.debug(f"Cannot download previous content archive [{err}]")
    try:
        os.mkdir(PUBLIC)
    except BaseException as err:
        logging.debug(f"Cannot make public directory [{err}]")

try:
    with open(os.path.join(PUBLIC, DATABASE),"r") as file:
        database = json.loads(file.read())
except BaseException as err:
    logging.debug(f"Cannot load database [{err}]")
    database = []

state = State.FAILURE
coverage_path = os.path.join("frontend", "coverage")
if os.path.isdir(coverage_path):
    logging.debug(f"Current commit is coverage success")
    shutil.copytree(coverage_path, os.path.join(PUBLIC, CI_COMMIT_SHA))
    state = State.OK
else:
    logging.debug(f"Current commit is coverage failure")

database.append({"commit": CI_COMMIT_SHA, "timestamp":CI_COMMIT_TIMESTAMP, "branch": CI_COMMIT_REF_NAME, "message": CI_COMMIT_MESSAGE, "state":state})

with open(os.path.join(PUBLIC, DATABASE),"w") as file:
    file.write(json.dumps(database))

with open(os.path.join(PUBLIC, "index.html"),"w") as file:
    file.write("<html><body>")

    for v in reversed(database):
        if v["state"] == State.OK:
            file.write(f"""
                <a href="{v["commit"]}">
                <img src="{SHIELD_BADGE_URL}/branch-{v["branch"]}-blue.svg" alt=""/>
                <img src="{SHIELD_BADGE_URL}/commit-{v["commit"]}-green.svg" alt=""/>
                </a> {v["timestamp"]}
                <p>{v["message"]}</p>
                """)
        else:
            file.write(f"""
                <img src="{SHIELD_BADGE_URL}/branch-{v["branch"]}-blue.svg" alt=""/>
                <img src="{SHIELD_BADGE_URL}/commit-{v["commit"]}-red.svg" alt=""/>
                {v["timestamp"]}
                <p>{v["message"]}</p>
                """)

    file.write("</body></html>")

if os.path.exists(CONTENT):
  os.remove(CONTENT)
with tarfile.open(CONTENT, "w:gz") as tar:
    tar.add(PUBLIC, arcname=os.path.basename(PUBLIC))
shutil.move(CONTENT, os.path.join(PUBLIC, CONTENT))