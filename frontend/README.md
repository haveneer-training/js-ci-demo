# CI DEMO

* Install
```
npm install
```

* Run dev server
```
nom run start
```

* Run test (non interactive)
```
npm test -- --watchAll=false
```

* Run coverage (non interactive)
```
npm test -- --coverage --watchAll=false
```

* Check package vulnerabilities
```
npm audit
```