const demod = require('../demod');

test('adds 1 + 2 to equal 3', () => {
    expect(demod.sum(1, 2)).toBe(3);
});