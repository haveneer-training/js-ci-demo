function demod(a, b) {
    return a + b;
}

function more_code_without_test(a) {
    return a * a + a;
}

module.exports = { sum: demod, more_code_without_test }
